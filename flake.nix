{
  description = "Initial draft for flake-based Nix&R deployments.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pname = "runCalc";
        pkgs = nixpkgs.legacyPackages.${system};
        rsienaMod = pkgs.rPackages.buildRPackage {
          name = "rsiena";
          src = pkgs.fetchFromGitHub{
            owner = "Kaladani";
            repo = "rsienafork";
            rev = "37880171c1583f51765c10f0ff6885268103155d";
            hash = "sha256-w24qqI1ujTJHOOEmswW/b5dRG3dzwgyVXjD93LsuZTI=";
          };
          propagatedBuildInputs = with pkgs.rPackages; [ Matrix lattice MASS xtable ];
        };
        R-with-custom-packages = pkgs.rWrapper.override{ packages = with pkgs; [ R rsienaMod ]; };
        customBuildInputs = [
          R-with-custom-packages
        ];
        name = "run-calc";
        rRunCalc = (pkgs.writeScriptBin name (builtins.readFile ./test_rsiena_installation.R));

      in rec {
        defaultPackage = packages.${pname};
        packages = {
          RSiena = rsienaMod;
          ${pname} = pkgs.symlinkJoin {
            inherit name;
            paths = [ rRunCalc ] ++ customBuildInputs;
            buildInputs = [ pkgs.makeWrapper ];
            postBuild = "wrapProgram $out/bin/${name} --prefix PATH : $out/bin";
          };
        };
        devShells.default = pkgs.mkShell {
          packages = [ rRunCalc rsienaMod ] ++ customBuildInputs;
        };
      });
}
