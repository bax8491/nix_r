# Nix ♥️ R

Dieses Repository dient dazu, einmal zu demonstrieren wie Nix mit einem
R-Projekt interagieren könnte. Bisher liegt hier nur eine `flake.nix`,
die eine Entwicklungsumgebung mit einigen, beliebigen R-Paketen zur
Verfügung stellt. Auch verwende ich derzeit den `unstable-release` von
Nix, welches sich nicht für den wissenschaftlichen Bereich eignen
dürfte.

Für eine Verwendung ist `nix` erforderlich. Wie das geht, steht
[hier](https://nixos.org/download.html). Darüber hinaus müssen die
experimentellen Features `nix-command` und `flakes` aktiviert sein.
Erklärung
[hier](https://nixos.wiki/wiki/Flakes#Other_Distros:_Without_Home-Manager).

Es gibt mehrere Möglichkeiten, die Umgebung auszuführen:

1.  Direktes Ausführen des Scripts, ohne das Repository selbst herunterzuladen:

    ``` bash
    nix run git+https://gitlab.rrz.uni-hamburg.de/bax8491/nix_r
    ```

    Damit wird die Umgebung aktiviert, das R-Skript ausgeführt und das
    Ergebnis in die Kommandozeile ausgegeben.

2.  Direktes Ausführen des beiliegenden, lokal gespeicherten Scripts, wenn das
    Repository vorher heruntergeladen wurde und man sich mit dem Terminal darin
    befindet:

    ``` bash
    nix run .
    ```

    Damit wird die Umgebung aktiviert, das R-Skript ausgeführt und das
    Ergebnis in die Kommandozeile ausgegeben.

3.  Mit

    ``` bash
    nix develop
    ```

    lässt sich die Umgebung aktivieren. Ab dort sollten alle Abhängigkeiten zur
    Verfügung stehen und man kann das `Rscript` mit folgendem Befehl ausführen:

    ``` bash
    run-script
    ```


In allen Fällen sollte Folgendes ausgegeben werden, um einen erfolgreichen Ablauf sicherzustellen:

``` 
[1] "RSiena version 1.4.13 could be loaded."
If you use this algorithm object, siena07 will create/use an output file /tmp/RtmpWRRENo/Sienaf49b5364b8bc.txt .
This is a temporary file for this R session.
[1] "RSiena::siena07 runs without error"
[1] "Target statistics have been calculated correctly"
```


# Probleme bei der Ausführung unter Windows WSL

Anscheinend gibt Nix bei der ersten Ausführung von `nix run` unter Windows WSL
einen Permission-Fehler zurück. Dies lässt sich umgehen, indem zuerst `nix
develop` ausgeführt wird. Danach sollte `nix run` regulär funktionieren.

# Direnv

Für eine automatische Aktivierung der Nix-Umgebung wird hier `direnv`
verwendet. Wer das verwenden möchte, findet in dem Packagemanager
seines:ihres Vertrauens die jeweilige Distribution. Damit stehen direkt
beim Eintritt in den Ordner das Äquivalent von `nix develop` zur
Verfügung.

# Probleme mit dem RStudio-Wrapper

Bisher gibt es noch ein Problem mit RStudio, welches zwar ebenfalls in
der Flake definiert ist, allerdings bisher mit folgendem Fehler bei mir
nicht startet:

``` bash
/nix/store/nn617dg6i7nfrgihawwzspz8cljnx3qq-qtwebengine-5.15.13/libexec/QtWebEngineProcess: error while loading shared libraries: __vdso_gettimeofday: invalid mode for dlopen(): Invalid argument
/nix/store/nn617dg6i7nfrgihawwzspz8cljnx3qq-qtwebengine-5.15.13/libexec/QtWebEngineProcess: error while loading shared libraries: __vdso_gettimeofday: invalid mode for dlopen(): Invalid argument
[1]    57030 trace trap (core dumped)  rstudio
```

Meiner Recherche nach liegt das vermutlich an den dynamisch verknüpften
Libraries, welche dann von der `qtwebengine` nicht gefunden werden
können. Lösungsvorschläge dazu sind allerdings eher dürftig.
Perspektivisch, soweit eine Nutzung erforderlich sein sollte, müsste ich
das im Nix-Forum einmal zur Sprache bringen.

Disclaimer: Meine derzeitige Linux-Installation ist in einem desolaten
Zustand, weshalb ich nicht garantieren kann, dass das Problem nicht von
meiner Konfiguration abhängt.
